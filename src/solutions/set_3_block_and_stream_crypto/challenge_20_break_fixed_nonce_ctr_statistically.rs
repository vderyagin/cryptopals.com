// https://cryptopals.com/sets/3/challenges/20

use utils;
use aes::ctr;
use base64;

const BLOCK_SIZE: usize = 16;
const DATA: &'static [u8] = include_bytes!("20.txt");

fn cyphertexts() -> Vec<Vec<u8>> {
  let key = utils::random_bytes(BLOCK_SIZE);
  DATA
    .split(|&byte| byte == b'\n')
    .map(|base64_input| base64::decode_bytes(base64_input).unwrap())
    .map(|data| ctr::encrypt(&data, &key, 0))
    .collect()
}
