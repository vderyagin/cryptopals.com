// https://cryptopals.com/sets/3/challenges/17

use aes::cbc;
use base64;
use pkcs7;
use rand::distributions::{Sample, Range};
use rand;
use utils;

const BLOCK_SIZE: usize = 16;
const INPUTS: &'static [&'static [u8]] =
  &[b"MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=",
    b"MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=",
    b"MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==",
    b"MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==",
    b"MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl",
    b"MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==",
    b"MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==",
    b"MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=",
    b"MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=",
    b"MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93"];

pub fn generate_encryption_function(key: &[u8]) -> Box<Fn() -> (Vec<u8>, Vec<u8>)> {
  let key = key.to_vec();

  Box::new(move || {
    let iv = utils::random_bytes(BLOCK_SIZE);
    let idx = Range::new(0, INPUTS.len()).sample(&mut rand::thread_rng());
    let data = base64::decode_bytes(INPUTS[idx]).unwrap();
    let ciphertext = cbc::encrypt(&pkcs7::pad(&data, BLOCK_SIZE), &key, &iv);
    (ciphertext, iv)
  })
}

pub fn decrypt(ciphertext: &[u8], iv: &[u8], oracle: &Box<Fn(&[u8], &[u8]) -> bool>) -> Vec<u8> {
  [iv, ciphertext]
    .concat()
    .chunks(BLOCK_SIZE)
    .collect::<Vec<_>>()
    .windows(2)
    .flat_map(|win| cbc::decrypt_block_with_padding_oracle(win[1], win[0], oracle))
    .collect()
}

#[cfg(test)]
mod tests {
  use utils;
  use pkcs7;
  use base64;
  use aes::cbc;
  use super::{
    generate_encryption_function,
    decrypt,
    BLOCK_SIZE,
    INPUTS,
  };

  #[test]
  fn successfully_recovers_plaintext() {
    let key = utils::random_bytes(BLOCK_SIZE);
    let encryptor = generate_encryption_function(&key);
    let padding_oracle = cbc::make_padding_oracle(&key);

    for _ in 0..10 {                       // enough iteration to use most inputs
      let (ciphertext, iv) = encryptor();
      let padded_plaintext = decrypt(&ciphertext, &iv, &padding_oracle);
      let plaintext = pkcs7::unpad(&padded_plaintext);
      assert!(INPUTS.contains(&base64::encode(&plaintext).as_bytes()));
    }
  }
}
