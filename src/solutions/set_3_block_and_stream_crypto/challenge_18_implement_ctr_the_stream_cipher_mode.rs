// https://cryptopals.com/sets/3/challenges/18

pub const DATA: &'static [u8] =
  b"L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==";
pub const KEY: &'static [u8] = b"YELLOW SUBMARINE";
pub const NONCE: u64 = 0;

#[cfg(test)]
mod tests {
  use base64;
  use aes::ctr;
  use super::{KEY, DATA, NONCE};

  #[test]
  fn works_for_provided_example() {
    assert_eq!(ctr::encrypt(&base64::decode_bytes(DATA).unwrap(), KEY, NONCE),
               b"Yo, VIP Let's kick it Ice, Ice, baby Ice, Ice, baby ".to_vec());
  }
}
