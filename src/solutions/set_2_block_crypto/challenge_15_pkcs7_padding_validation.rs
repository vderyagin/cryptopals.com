// https://cryptopals.com/sets/2/challenges/15

#[cfg(test)]
mod tests {
  use pkcs7;

  #[test]
  fn works_for_provided_examples() {
    assert_eq!(pkcs7::unpad(b"ICE ICE BABY\x04\x04\x04\x04"),
               b"ICE ICE BABY");
  }

  #[test]
  #[should_panic]
  fn fails_for_provided_invalid_data_1() {
    pkcs7::unpad(b"ICE ICE BABY\x05\x05\x05\x05");
  }

  #[test]
  #[should_panic]
  fn fails_for_provided_invalid_data_2() {
    pkcs7::unpad(b"ICE ICE BABY\x01\x02\x03\x04");
  }
}
