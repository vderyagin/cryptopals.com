// https://cryptopals.com/sets/2/challenges/14

use aes::ecb;
use pkcs7;
use rand::distributions::{Sample, Range};
use rand;
use utils;

const BLOCK_SIZE: usize = 16;

pub fn generate_encryption_function(secret: &[u8]) -> Box<Fn(&[u8]) -> Vec<u8>> {
  let mut rng = rand::thread_rng();
  let key = utils::random_bytes(BLOCK_SIZE);
  let prefix = utils::random_bytes(Range::new(8, 20).sample(&mut rng));
  let secret = secret.to_vec();

  Box::new(move |data| {
    let plaintext = [prefix.as_slice(), data, secret.as_slice()].concat();
    ecb::encrypt(&pkcs7::pad(&plaintext, BLOCK_SIZE), &key)
  })
}

pub fn discover_prefix_size(encryptor: &Box<Fn(&[u8]) -> Vec<u8>>) -> usize {
  let mut plaintext = Vec::with_capacity(BLOCK_SIZE);

  let last_prefix_block = encryptor(b"aaa").as_slice().chunks(BLOCK_SIZE)
    .zip(encryptor(b"bbb").as_slice().chunks(BLOCK_SIZE))
    .enumerate()
    .find(|&(_, (a, b))| a != b)
    .unwrap()
    .0;

  loop {
    let mut plaintext_a = plaintext.to_vec();
    let mut plaintext_b = plaintext.to_vec();

    plaintext_a.push(b'a');
    plaintext_b.push(b'b');

    let ciphertext_a = encryptor(&plaintext_a);
    let ciphertext_b = encryptor(&plaintext_b);

    let block_a = utils::extract_block(&ciphertext_a, last_prefix_block, BLOCK_SIZE);
    let block_b = utils::extract_block(&ciphertext_b, last_prefix_block, BLOCK_SIZE);

    if block_a == block_b {
      break;
    }

    plaintext.push(b'x')
  }

  last_prefix_block * BLOCK_SIZE + BLOCK_SIZE - plaintext.len()
}

/// Produce a function that works like `encryptor`, except:
/// * it prepends `pad` bytes to input data before encryption
/// * drops `drop` bytes of resulting ciphertext
pub fn truncate_encryptor(encryptor: Box<Fn(&[u8]) -> Vec<u8>>, pad: usize, drop: usize) -> Box<Fn(&[u8]) -> Vec<u8>> {
  Box::new(move |data| {
    let mut plaintext = Vec::with_capacity(pad + data.len());
    plaintext.resize(pad, 0);
    plaintext.extend_from_slice(data);
    encryptor(&plaintext)[drop..].to_vec()
  })
}

pub fn recover_secret(encryptor: Box<Fn(&[u8]) -> Vec<u8>>) -> Vec<u8> {
  let prefix_size = discover_prefix_size(&encryptor);
  let pad_size = BLOCK_SIZE - prefix_size % BLOCK_SIZE;
  let truncated_encryptor =
    truncate_encryptor(encryptor, pad_size, prefix_size + pad_size);
  ecb::recover_suffix(&truncated_encryptor)
}

#[cfg(test)]
mod tests {
  use super::{generate_encryption_function, recover_secret};
  use utils;

  #[test]
  fn recovers_the_secret() {
    let secret = utils::random_bytes(50);
    let encryptor = generate_encryption_function(&secret);

    assert_eq!(recover_secret(encryptor), secret);
  }
}
