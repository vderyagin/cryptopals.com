// https://cryptopals.com/sets/2/challenges/12

use aes::ecb;
use base64;
use pkcs7;
use utils;

const BLOCK_SIZE: usize = 16;
const UNKNOWN_DATA_BASE64: &'static [u8] =
  b"Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg
aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq
dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg
YnkK";

pub fn generate_encryption_function() -> Box<Fn(&[u8]) -> Vec<u8>> {
  let key = utils::random_bytes(BLOCK_SIZE);
  let unknown_data = base64::decode_bytes(UNKNOWN_DATA_BASE64).unwrap();

  Box::new(move |data| {
    let plaintext = [data, &unknown_data].concat();
    ecb::encrypt(&pkcs7::pad(&plaintext, BLOCK_SIZE), &key)
  })
}

pub fn discover_block_size(func: &Box<Fn(&[u8]) -> Vec<u8>>) -> usize {
  let mut plaintext = Vec::with_capacity(1000);
  let initial_size = func(&plaintext).len();

  for _ in 0.. {
    let new_size = func(&plaintext).len();
    if new_size != initial_size {
      return new_size - initial_size;
    }
    plaintext.push(0);
  }

  panic!("should be unreachable")
}

#[cfg(test)]
mod tests {
  use base64;
  use aes::ecb;
  use super::{discover_block_size,
             generate_encryption_function,
             UNKNOWN_DATA_BASE64,
             BLOCK_SIZE};

  #[test]
  fn determines_the_block_size_used() {
    assert_eq!(BLOCK_SIZE, discover_block_size(&generate_encryption_function()));
  }

  #[test]
  fn determines_that_ecb_mode_is_used() {
    assert!(ecb::oracle(generate_encryption_function()));
  }

  #[test]
  fn works_for_provided_example() {
    assert_eq!(ecb::recover_suffix(&generate_encryption_function()),
               base64::decode_bytes(UNKNOWN_DATA_BASE64).unwrap());
  }
}
