// https://cryptopals.com/sets/2/challenges/11

use rand::distributions::{Sample, Range};
use rand;

use aes::{cbc, ecb};
use pkcs7;
use utils;

const BLOCK_SIZE: usize = 16;

fn pad(data: &[u8]) -> Vec<u8> {
  let mut rng = rand::thread_rng();
  let mut pad_range = Range::new(5, 11);

  let padded = [
    &utils::random_bytes(pad_range.sample(&mut rng)),
    data,
    &utils::random_bytes(pad_range.sample(&mut rng))
  ].concat();

  pkcs7::pad(&padded, BLOCK_SIZE)
}

#[derive(Debug, PartialEq)]
pub enum Mode { CBC, ECB, Random }

pub fn encrypt(data: &[u8], mode: Mode) -> Vec<u8> {
  let key = utils::random_bytes(BLOCK_SIZE);
  let padded = pad(data);

  match mode {
    Mode::CBC => cbc::encrypt(&padded, &key, &utils::random_bytes(BLOCK_SIZE)),
    Mode::ECB => ecb::encrypt(&padded, &key),
    Mode::Random => {
      let mode = if rand::random::<bool>() { Mode::ECB } else { Mode::CBC };
      encrypt(data, mode)
    },
  }
}

pub fn oracle<F>(func: F) -> Mode where F: Fn(&[u8]) -> Vec<u8> + 'static {
  if ecb::oracle(Box::new(func)) { Mode::ECB } else { Mode::CBC }
}

#[cfg(test)]
  mod tests {
  use super::{oracle, encrypt, Mode};

  #[test]
  fn guesses_cbc_every_time() {
    for _ in 0..20 {
      assert_eq!(oracle(|data| encrypt(data, Mode::CBC)), Mode::CBC)
    }
  }

  #[test]
  fn guesses_ecb_every_time() {
    for _ in 0..20 {
      assert_eq!(oracle(|data| encrypt(data, Mode::ECB)), Mode::ECB)
    }
  }

  #[test]
  fn guesses_both_modes_when_randomly_selected() {
    let guessed_modes = (0..20)
      .map(|_| oracle(|data| encrypt(data, Mode::Random)))
      .collect::<Vec<_>>();

    assert!(guessed_modes.iter().any(|mode| *mode == Mode::ECB));
    assert!(guessed_modes.iter().any(|mode| *mode == Mode::CBC));
  }
}
