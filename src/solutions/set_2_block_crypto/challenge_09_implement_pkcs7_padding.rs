// https://cryptopals.com/sets/2/challenges/9

#[cfg(test)]
mod tests {
  use pkcs7;

  #[test]
  fn works_for_provided_example() {
    let text = b"YELLOW SUBMARINE";
    let padded_text = b"YELLOW SUBMARINE\x04\x04\x04\x04";
    assert_eq!(pkcs7::pad(text, 20).as_slice(), padded_text);
  }
}
