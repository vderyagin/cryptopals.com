// https://cryptopals.com/sets/2/challenges/13

use aes::ecb;
use pkcs7;
use std::collections::HashMap;
use std::{self, fmt, str};
use utils;

const BLOCK_SIZE: usize = 16;

#[derive(Debug, PartialEq)]
pub enum Role { User, Admin }

impl Role {
  fn from_str(role: &str) -> Self {
    match role {
      "user"  => Role::User,
      "admin" => Role::Admin,
      _       => panic!("invalid role (must be either 'user' or 'admin')"),
    }
  }
}

impl fmt::Display for Role {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}", match self {
      &Role::User  => "user",
      &Role::Admin => "admin",
    })
  }
}

#[derive(Debug, PartialEq)]
pub struct User {
  email: String,
  uid:   usize,
  role:  Role,
}

impl User {
  pub fn from_email(email: &str) -> Self {
    if email.chars().any(|ch| ch == '&' || ch == '=') {
      panic!("Email can not include characters '&' and '='");
    }

    User{
      email: email.to_string(),
      uid:   10,
      role:  Role::User,
    }
  }

  pub fn encode(&self) -> String {
    format!("email={}&uid={}&role={}", self.email, self.uid, self.role)
  }

  pub fn decode(encoded: &[u8]) -> Self {
    let map = keywords_parser(encoded).unwrap().1;

    User{
      email: map.get("email").unwrap().to_string(),
      uid:   map.get("uid").unwrap().parse().unwrap(),
      role:  Role::from_str(map.get("role").unwrap()),
    }
  }

  pub fn is_admin(&self) -> bool {
    self.role == Role::Admin
  }
}

named!(pair<&[u8], (&str, &str)>, separated_pair!(
  map_res!(is_not!("="), std::str::from_utf8),
  tag!("="),
  map_res!(is_not!("&"), std::str::from_utf8)
));

named!(keywords_parser<&[u8], HashMap<&str, &str> >,
       map!(separated_list!(tag!("&"), pair), pairs_to_hash_map));

fn pairs_to_hash_map<'a>(pairs: Vec<(&'a str, &'a str)>) -> HashMap<&'a str, &'a str> {
  pairs.iter().map(|&(a, b)| (a, b)).collect()
}

pub fn profile_for(email: &str) -> String {
  User::from_email(email).encode()
}

pub fn generate_profile_encryption_function(key: &[u8]) -> Box<Fn(&[u8]) -> Vec<u8>> {
  let key = key.to_vec();
  Box::new(move |email| {
    let encoded_profile = profile_for(str::from_utf8(email).unwrap());
    let padded_plaintext = pkcs7::pad(encoded_profile.as_bytes(), BLOCK_SIZE);
    ecb::encrypt(&padded_plaintext, &key)
  })
}

pub fn decrypt_profile(ciphertext: &[u8], key: &[u8]) -> User {
  let raw_plaintext = ecb::decrypt(ciphertext, key);
  User::decode(pkcs7::unpad(&raw_plaintext))
}

pub fn forge_admin_profile_ciphertext(encryptor: &Box<Fn(&[u8]) -> Vec<u8>>) -> Vec<u8> {
  let last_block = pkcs7::pad(b"admin", BLOCK_SIZE);
  let email_length = BLOCK_SIZE - b"email=".len();

  let mut email = Vec::with_capacity(BLOCK_SIZE + email_length);
  email.resize(email_length, b'X');
  email.extend_from_slice(&last_block);

  let mut ciphertext = encryptor(&email);

  let last_block_ciphertext = utils::extract_block(&ciphertext, 1, BLOCK_SIZE).to_vec();

  let other_stuff_length = b"email=&uid=10&role=".len();
  let new_email_length =
    (other_stuff_length / BLOCK_SIZE + 1) * BLOCK_SIZE - other_stuff_length;

  email.resize(new_email_length, b'X');
  ciphertext = encryptor(&email);

  [&ciphertext[..(2 * BLOCK_SIZE)], &last_block_ciphertext].concat()
}

#[cfg(test)]
mod tests {
  use utils;
  use super::{BLOCK_SIZE,
             generate_profile_encryption_function,
             decrypt_profile,
             forge_admin_profile_ciphertext};

  mod keywords_parser {
    use super::super::keywords_parser;

    #[test]
    fn parses_provided_example() {
      let map = keywords_parser(b"foo=bar&baz=qux&zap=zazzle").unwrap().1;
      assert_eq!(map.get("foo"), Some(&"bar"));
      assert_eq!(map.get("baz"), Some(&"qux"));
      assert_eq!(map.get("zap"), Some(&"zazzle"));
    }
  }

  #[test]
  fn works_for_provided_example() {
    let key = utils::random_bytes(BLOCK_SIZE);
    let encryptor = generate_profile_encryption_function(&key);
    let ciphertext = forge_admin_profile_ciphertext(&encryptor);
    let user = decrypt_profile(&ciphertext, &key);

    assert!(user.is_admin());
  }
}
