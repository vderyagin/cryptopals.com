// https://cryptopals.com/sets/2/challenges/16

use aes::cbc;
use pkcs7;
use utils;

const BLOCK_SIZE: usize = 16;

fn wrap(data: &[u8]) -> Vec<u8> {
  if data.iter().any(|&ch| ch == b';' || ch == b'=') {
    panic!("input should not include ';' or '=' characters");
  }

  let prefix = b"comment1=cooking%20MCs;userdata=";
  let suffix = b";comment2=%20like%20a%20pound%20of%20bacon";

  [prefix, data, suffix].concat()
}

pub fn generate_encryption_function(key: &[u8], iv: &[u8]) -> Box<Fn(&[u8]) -> Vec<u8>> {
  let key = key.to_vec();
  let iv = iv.to_vec();

  Box::new(move |data| {
    let plaintext = wrap(data);
    let padded_plaintext = pkcs7::pad(&plaintext, BLOCK_SIZE);
    cbc::encrypt(&padded_plaintext, &key, &iv)
  })
}

pub fn forge_ciphertext(encryptor: &Box<Fn(&[u8]) -> Vec<u8>>) -> Vec<u8> {
  let mut plaintext = b"xx;admin=true;xx".to_vec();
  let prefix_size = 2 * BLOCK_SIZE;
  let naughty_char_idxs = &[2, 8, 13];    // two semicolons and "equals" sign

  for &idx in naughty_char_idxs {
    plaintext[idx] ^= 1;
  }

  let mut ciphertext = encryptor(&plaintext);

  for &idx in naughty_char_idxs {
    ciphertext[prefix_size - BLOCK_SIZE + idx] ^= 1;
  }

  ciphertext
}

pub fn is_forged(ciphertext: &[u8], key: &[u8], iv: &[u8]) -> bool {
  let plaintext = cbc::decrypt(ciphertext, key, iv);
  let unpadded_plaintext = pkcs7::unpad(&plaintext);
  utils::contains(unpadded_plaintext, b";admin=true;")
}

#[cfg(test)]
mod tests {
  use utils;
  use super::{generate_encryption_function,
             is_forged,
             forge_ciphertext,
             BLOCK_SIZE};

  #[test]
  fn successfully_forges_ciphertext() {
    let key = utils::random_bytes(BLOCK_SIZE);
    let iv = utils::random_bytes(BLOCK_SIZE);
    let ciphertext = forge_ciphertext(&generate_encryption_function(&key, &iv));
    assert!(is_forged(&ciphertext, &key, &iv));
  }
}
