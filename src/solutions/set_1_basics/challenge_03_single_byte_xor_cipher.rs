// https://cryptopals.com/sets/1/challenges/3

#[cfg(test)]
mod tests {
  use single_byte_xor;
  use hex;

  #[test]
  fn works_for_provided_example() {
    let cipher_hex = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
    let plaintext = "Cooking MC's like a pound of bacon".to_string();
    let key = 88;
    assert_eq!(Some((plaintext, key)),
               single_byte_xor::decrypt(&hex::decode(cipher_hex).unwrap()));
  }
}
