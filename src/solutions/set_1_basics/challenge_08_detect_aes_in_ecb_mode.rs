// https://cryptopals.com/sets/1/challenges/8

use hex;
use utils;

const DATA: &'static [u8] = include_bytes!("8.txt");
const BLOCK_SIZE: usize = 16;

fn inputs() -> Vec<Vec<u8>> {
  DATA
    .split(|byte| *byte == b'\n')
    .map(|data| hex::decode_bytes(data).unwrap())
    .collect()
}

pub fn detect_ecb() -> Vec<u8> {
  inputs()
    .iter()
    .max_by_key(|d| utils::count_duplicate_blocks(d, BLOCK_SIZE))
    .unwrap()
    .to_vec()
}

#[cfg(test)]
mod tests {
  use super::detect_ecb;
  use hex;

  #[test]
  fn works_for_provided_example() {
    let ecb_hex = "d880619740a8a19b7840a8a31c810a3d08649af70dc06f4fd5d2d69c744cd283e2dd052f6b641dbf9d11b0348542bb5708649af70dc06f4fd5d2d69c744cd2839475c9dfdbc1d46597949d9c7e82bf5a08649af70dc06f4fd5d2d69c744cd28397a93eab8d6aecd566489154789a6b0308649af70dc06f4fd5d2d69c744cd283d403180c98c8f6db1f2a3f9c4040deb0ab51b29933f2c123c58386b06fba186a";
    assert_eq!(hex::decode(ecb_hex).unwrap(), detect_ecb());
  }
}
