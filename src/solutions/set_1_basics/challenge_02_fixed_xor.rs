// https://cryptopals.com/sets/1/challenges/2

use hex;

pub fn fixed_xor(hex_1: &str, hex_2: &str) -> String {
  let buf_1 = hex::decode(hex_1).unwrap();
  let buf_2 = hex::decode(hex_2).unwrap();

  hex::encode(&buf_1.iter().zip(buf_2)
             .map(|(x, y)| x ^ y)
             .collect::<Vec<_>>())
}

#[cfg(test)]
mod tests {
  use super::fixed_xor;

  #[test]
  fn it_works_for_provided_example() {
    let hex_1 = "1c0111001f010100061a024b53535009181c";
    let hex_2 = "686974207468652062756c6c277320657965";
    let xor = "746865206b696420646f6e277420706c6179";

    assert_eq!(&fixed_xor(hex_1, hex_2), xor);
  }
}
