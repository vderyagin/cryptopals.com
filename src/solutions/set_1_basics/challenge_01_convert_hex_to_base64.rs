// https://cryptopals.com/sets/1/challenges/1

use hex;
use base64;

pub fn hex2base64(hex: &str) -> String {
  base64::encode(&hex::decode(hex).unwrap())
}

#[cfg(test)]
mod tests {
  use super::hex2base64;

  #[test]
  fn works_for_provided_example() {
    let hex = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
    let base64 = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";

    assert_eq!(&hex2base64(hex), base64);
  }
}
