// https://cryptopals.com/sets/1/challenges/5

#[cfg(test)]
mod tests {
  use hex;
  use repeating_key_xor;

  #[test]
  fn works_for_provided_example() {
    let plaintext = b"Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal";
    let ciphertext_hex = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f";
    let ciphertext = repeating_key_xor::encrypt(plaintext, b"ICE");
    assert_eq!(ciphertext_hex, hex::encode(&ciphertext));
  }
}
