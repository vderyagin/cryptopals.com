// https://cryptopals.com/sets/1/challenges/4

pub const DATA: &'static [u8] = include_bytes!("4.txt");

pub fn inputs() -> Vec<&'static [u8]> {
  DATA.split(|byte| *byte == b'\n').collect()
}

#[cfg(test)]
mod tests {
  use super::inputs;
  use single_byte_xor;
  use hex;

  #[test]
  fn correct_number_of_inputs_is_read() {
    assert_eq!(inputs().len(), 327);
  }

  #[test]
  fn works_for_provided_example() {
    let decrypted = inputs()
      .iter()
      .map(|input| single_byte_xor::decrypt(&hex::decode_bytes(input).unwrap()))
      .filter(|res| res.is_some())
      .map(|res| res.unwrap())
      .map(|(text, _)| text)
      .collect::<Vec<_>>();

    assert_eq!(decrypted.len(), 1);
    assert_eq!(decrypted[0],  "Now that the party is jumping\n");
  }
}
