// https://cryptopals.com/sets/1/challenges/6

use num_rational::Ratio;

use hamming;
use base64;
use repeating_key_xor;
use single_byte_xor;

const DATA: &'static [u8] = include_bytes!("6.txt");

fn key_length_score(ciphertext: &[u8], length: usize) -> Ratio<usize> {
  let score = ciphertext
    .chunks(length)
    .take(20)
    .collect::<Vec<_>>()
    .as_slice()
    .windows(2)
    .map(|win| hamming::distance(win[0], win[1]))
    .sum();

  Ratio::new(score, length)
}

fn key_length(ciphertext: &[u8]) -> usize {
  (2..41)
    .min_by_key(|size| key_length_score(ciphertext, *size))
    .unwrap()
}

fn break_into_blocks(data: &[u8], block_size: usize) -> Vec<&[u8]> {
  let blocks = data.len() / block_size;
  data.chunks(block_size).take(blocks).collect()
}

fn transpose_blocks(blocks: &[&[u8]]) -> Vec<Vec<u8>> {
  if blocks.is_empty() { return Vec::new() };

  let block_size = blocks[0].len();

  if blocks.iter().any(|block| block.len() != block_size) {
    panic!("not all blocks have equal length");
  }

  let mut transposed = Vec::new();

  for _ in 0..block_size {
    transposed.push(Vec::new());
  }

  for block in blocks {
    for (idx, byte) in block.iter().enumerate() {
      transposed[idx].push(*byte)
    }
  }

  transposed
}

fn repeating_key(ciphertext: &[u8]) -> Vec<u8> {
  transpose_blocks(&break_into_blocks(ciphertext, key_length(ciphertext)))
    .iter()
    .map(|data| single_byte_xor::decrypt(data).unwrap())
    .map(|(_, key_byte)| key_byte)
    .collect()
}

pub fn decrypt(ciphertext: &[u8]) -> String {
  let key = repeating_key(ciphertext);
  let plain_text = repeating_key_xor::encrypt(ciphertext, &key);
  String::from_utf8(plain_text).unwrap()
}

pub fn do_decrypt() -> String {
  decrypt(&base64::decode_bytes(DATA).unwrap())
}

#[cfg(test)]
mod tests {
  use super::do_decrypt;

  #[test]
  fn works_for_provided_example() {
    let plain_text = "I'm back and I'm ringin' the bell \nA rockin' on the mike while the fly girls yell \nIn ecstasy in the back of me \nWell that's my DJ Deshay cuttin' all them Z's \nHittin' hard and the girlies goin' crazy \nVanilla's on the mike, man I'm not lazy. \n\nI'm lettin' my drug kick in \nIt controls my mouth and I begin \nTo just let it flow, let my concepts go \nMy posse's to the side yellin', Go Vanilla Go! \n\nSmooth 'cause that's the way I will be \nAnd if you don't give a damn, then \nWhy you starin' at me \nSo get off 'cause I control the stage \nThere's no dissin' allowed \nI'm in my own phase \nThe girlies sa y they love me and that is ok \nAnd I can dance better than any kid n' play \n\nStage 2 -- Yea the one ya' wanna listen to \nIt's off my head so let the beat play through \nSo I can funk it up and make it sound good \n1-2-3 Yo -- Knock on some wood \nFor good luck, I like my rhymes atrocious \nSupercalafragilisticexpialidocious \nI'm an effect and that you can bet \nI can take a fly girl and make her wet. \n\nI'm like Samson -- Samson to Delilah \nThere's no denyin', You can try to hang \nBut you'll keep tryin' to get my style \nOver and over, practice makes perfect \nBut not if you're a loafer. \n\nYou'll get nowhere, no place, no time, no girls \nSoon -- Oh my God, homebody, you probably eat \nSpaghetti with a spoon! Come on and say it! \n\nVIP. Vanilla Ice yep, yep, I'm comin' hard like a rhino \nIntoxicating so you stagger like a wino \nSo punks stop trying and girl stop cryin' \nVanilla Ice is sellin' and you people are buyin' \n'Cause why the freaks are jockin' like Crazy Glue \nMovin' and groovin' trying to sing along \nAll through the ghetto groovin' this here song \nNow you're amazed by the VIP posse. \n\nSteppin' so hard like a German Nazi \nStartled by the bases hittin' ground \nThere's no trippin' on mine, I'm just gettin' down \nSparkamatic, I'm hangin' tight like a fanatic \nYou trapped me once and I thought that \nYou might have it \nSo step down and lend me your ear \n'89 in my time! You, '90 is my year. \n\nYou're weakenin' fast, YO! and I can tell it \nYour body's gettin' hot, so, so I can smell it \nSo don't be mad and don't be sad \n'Cause the lyrics belong to ICE, You can call me Dad \nYou're pitchin' a fit, so step back and endure \nLet the witch doctor, Ice, do the dance to cure \nSo come up close and don't be square \nYou wanna battle me -- Anytime, anywhere \n\nYou thought that I was weak, Boy, you're dead wrong \nSo come on, everybody and sing this song \n\nSay -- Play that funky music Say, go white boy, go white boy go \nplay that funky music Go white boy, go white boy, go \nLay down and boogie and play that funky music till you die. \n\nPlay that funky music Come on, Come on, let me hear \nPlay that funky music white boy you say it, say it \nPlay that funky music A little louder now \nPlay that funky music, white boy Come on, Come on, Come on \nPlay that funky music \n";
    assert_eq!(plain_text, &do_decrypt());
  }

  mod transpose_blocks {
    use super::super::transpose_blocks;

    #[test]
    fn deals_with_empty_thing() {
      let empty: Vec<Vec<u8>> = Vec::new();
      assert_eq!(transpose_blocks(&[]), empty);
    }

    #[test]
    #[should_panic]
    fn panics_when_memebers_are_of_different_lengths() {
      transpose_blocks(&[&[1, 2, 3], &[1, 2]]);
    }

    #[test]
    fn transposes_valid_2_dimensional_array() {
      assert_eq!(transpose_blocks(&[&[1, 2, 3], &[4, 5, 6]]),
                 vec![vec![1, 4], vec![2, 5], vec![3, 6]]);
    }
  }
}
