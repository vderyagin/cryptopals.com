use std::ascii::AsciiExt;

use std;

fn single_byte_xor(plaintext: &[u8], key: u8) -> Vec<u8> {
  plaintext.iter().map(|b| b ^ key).collect::<Vec<_>>()
}

fn score_english_text(text: &str) -> usize {
  [
    char_frequency(text, 'e') > char_frequency(text, 't'),
    char_frequency(text, 't') > char_frequency(text, 'r'),
    char_frequency(text, 'a') > char_frequency(text, 'b'),
    char_frequency(text, 'a') > char_frequency(text, 'c'),
    char_frequency(text, 'o') > char_frequency(text, 'u'),
    char_frequency(text, 's') > char_frequency(text, 'j'),
    char_frequency(text, 'i') > char_frequency(text, 'k'),
    char_frequency(text, 'e') > char_frequency(text, 'c'),
    char_frequency(text, 'i') > char_frequency(text, 'g'),
  ].iter().filter(|x| **x).count()
}

fn char_frequency(text: &str, letter: char) -> usize {
  text.chars().filter(|c| c.to_lowercase().any(|lc| lc == letter)).count()
}

/// determine if input is likely to be English text
fn is_english(text: &str) -> bool {
  if !text.chars().any(|c| c == ' ') {
    return false;
  }

  if text.chars().any(|c| c != '\n' && c.is_control()) {
    return false;
  }

  let len = text.len();

  let alpha_len = text
    .chars()
    .filter(|c| c.is_alphabetic() || *c == ' ')
    .count();

  (alpha_len as f64 / len as f64) > 0.8
}

fn is_ascii(bytes: &[u8]) -> bool {
  if let Ok(s) = std::str::from_utf8(bytes) {
    s.is_ascii()
  } else {
    false
  }
}

pub fn decrypt(ciphertext: &[u8]) -> Option<(String, u8)> {
  (0..256)
    .map(|key| key as u8)
    .map(|key| (single_byte_xor(ciphertext, key), key))
    .filter(|&(ref d, _)| is_ascii(d))
    .map(|(d, k)| (std::str::from_utf8(&d).unwrap().to_string(), k))
    .filter(|&(ref s, _)| is_english(s))
    .max_by_key(|&(ref s, _)| score_english_text(s))
}
