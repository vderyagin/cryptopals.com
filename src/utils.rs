use rand;

pub fn random_bytes(size: usize) -> Vec<u8> {
  (0..size).map(|_| rand::random::<u8>()).collect()
}

pub fn count_duplicate_blocks(data: &[u8], block_size: usize) -> usize {
  data.chunks(block_size)
    .map(|ch| data.chunks(block_size).filter(|c| *c == ch).count() - 1)
    .sum::<usize>() / 2
}

pub fn extract_block(data: &[u8], idx: usize, block_size: usize) -> &[u8] {
  if data.len() < (idx + 1) * block_size {
    panic!("data is not long enough ")
  }

  data.chunks(block_size).nth(idx).unwrap()
}

pub fn contains<T: PartialEq<T>>(a: &[T], b: &[T]) -> bool {
  if a.len() < b.len() {
    return false;
  }

  let mut start_idx = 0;
  let mut end_idx = b.len();

  while end_idx < a.len() {
    if &a[start_idx..end_idx] == b {
      return true;
    }

    start_idx += 1;
    end_idx += 1;
  }

  false
}

pub fn xor(data_a: &[u8], data_b: &[u8]) -> Vec<u8> {
  data_a
    .iter()
    .zip(data_b)
    .map(|(a, b)| a ^ b)
    .collect()
}

#[cfg(test)]
mod tests {
  mod random_bytes {
    use super::super::random_bytes;

    #[test]
    fn returns_given_amount_of_bytes() {
      assert_eq!(random_bytes(8).len(), 8);
    }

    #[test]
    fn does_not_return_same_stuff_again() {
      assert!(random_bytes(10) != random_bytes(10));
    }
  }

  mod count_duplicate_blocks {
    use super::super::count_duplicate_blocks;

    #[test]
    fn zero_if_no_duplicates() {
      assert_eq!(count_duplicate_blocks(&[1, 2, 3, 4, 5, 6, 7, 8], 2), 0);
    }

    #[test]
    fn count_duplicates_of_single_block() {
      assert_eq!(count_duplicate_blocks(&[1, 2, 3, 4, 1, 2, 7, 8], 2), 1);
      assert_eq!(count_duplicate_blocks(&[1, 1, 1], 1), 3);
    }

    #[test]
    fn count_duplicates_of_multiple_different_blocks() {
      assert_eq!(count_duplicate_blocks(&[1, 2, 3, 4, 1, 2, 3, 4], 2), 2);
    }
  }

  mod extract_block {
    use super::super::extract_block;

    #[test]
    #[should_panic]
    fn panics_if_give_block_does_not_exist() {
      extract_block(b"", 1, 2);
    }

    #[test]
    #[should_panic]
    fn panics_if_given_block_is_not_full() {
      extract_block(b"123", 1, 2);
    }

    #[test]
    fn extracts_the_blocks() {
      assert_eq!(extract_block(b"123456", 0, 2), b"12");
      assert_eq!(extract_block(b"123456", 1, 2), b"34");
      assert_eq!(extract_block(b"123456", 2, 2), b"56");
    }
  }

  mod contains {
    use super::super::contains;

    #[test]
    fn returns_true_if_one_slice_includes_another() {
     assert!(contains(b"foobarbaz", b"bar"));
    }

    #[test]
    fn returns_false_if_one_slice_does_not_contain_the_other_one() {
      assert!(!contains(b"foobarbaz", b"quux"));
      assert!(!contains(b"foobarbaz", b"quuxddddddd"));
    }
  }
}
