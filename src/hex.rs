use std::str;

fn is_hex(ch: char) -> bool {
  (ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F')
}

pub fn decode_bytes(bytes: &[u8]) -> Result<Vec<u8>, &str> {
  if bytes.len() % 2 != 0 {
    return Err("invalid hex string (odd length)");
  }

  if !str::from_utf8(bytes).unwrap().chars().all(is_hex) {
    return Err("invalid hex string (has non-hex chars)");
  }

  Ok(bytes.chunks(2).map(|chunk| {
    u8::from_str_radix(str::from_utf8(chunk).unwrap(), 16).unwrap()
  }).collect())
}

pub fn decode(hex: &str) -> Result<Vec<u8>, &str> {
  decode_bytes(hex.as_bytes())
}

pub fn encode(data: &[u8]) -> String {
  data.iter().map(|byte| format!("{:02x}", byte)).collect()
}

#[cfg(test)]
mod tests {
  mod is_hex {
    use super::super::is_hex;

    #[test]
    fn works_for_hexadecimal_digits() {
      assert!(is_hex('0'));
      assert!(is_hex('3'));
      assert!(is_hex('9'));
      assert!(is_hex('a'));
      assert!(is_hex('A'));
      assert!(is_hex('f'));
      assert!(is_hex('F'));
    }

    #[test]
    fn does_not_work_for_non_hex_chars() {
      assert!(!is_hex('x'));
      assert!(!is_hex('©'));
      assert!(!is_hex('g'));
      assert!(!is_hex('-'));
    }
  }

  mod decode {
    use super::super::decode;

    #[test]
    fn works_for_empty_string() {
      assert_eq!(decode("").unwrap(), vec![]);
    }

    #[test]
    fn works_for_single_byte_string() {
      assert_eq!(decode("00").unwrap(), vec![0]);
      assert_eq!(decode("01").unwrap(), vec![1]);
      assert_eq!(decode("ff").unwrap(), vec![255]);
    }

    #[test]
    fn works_for_hex_strings_of_arbitrary_length() {
      assert_eq!(decode("0000").unwrap(), vec![0, 0]);
      assert_eq!(decode("ff00ff").unwrap(), vec![255, 0, 255]);
    }

    #[test]
    #[should_panic]
    fn panics_when_invoked_with_string_of_odd_size() {
      decode("0").unwrap();
    }

    #[test]
    #[should_panic]
    fn panics_when_invoked_with_string_that_has_non_hex_chars() {
      decode("xx").unwrap();
    }
  }

  mod encode {
    use super::super::encode;

    #[test]
    fn works_for_empty_input() {
      assert_eq!(&encode(&[]), "");
    }

    #[test]
    fn works_for_single_byte() {
      assert_eq!(&encode(&[255]), "ff");
      assert_eq!(&encode(&[0]), "00");
    }

    #[test]
    fn works_for_multi_byte_sequences() {
      assert_eq!(&encode(&[1, 2, 3, 255, 0]), "010203ff00");
    }
  }
}
