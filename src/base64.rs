use std::collections::HashMap;

static TABLE: &'static [char] =
  &['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'];

fn reverse_table() -> HashMap<char, u8> {
  TABLE.iter().enumerate().map(|(idx, &ch)| (ch, idx as u8)).collect()
}

pub fn encode(bytes: &[u8]) -> String {
  bytes.chunks(3).fold(
    String::with_capacity((bytes.len() / 3 + 1) * 4), |mut base64, chunk| {
      let chunk_size = chunk.len();

      let first_idx = chunk[0] >> 2;
      let mut second_idx = chunk[0] << 6 >> 2;

      if chunk_size > 1 {
        second_idx += chunk[1] >> 4
      }

      let mut third_idx = None;

      if chunk_size > 1 {
        let mut idx = chunk[1] << 4 >> 2;
        if chunk_size == 3 {
          idx += chunk[2] >> 6
        }
        third_idx = Some(idx)
      }

      let fourth_idx = if chunk_size == 3 {
        Some(chunk[2] << 2 >> 2)
      } else {
        None
      };

      base64.push(TABLE[first_idx as usize]);
      base64.push(TABLE[second_idx as usize]);

      if let Some(idx) = third_idx {
        base64.push(TABLE[idx as usize]);
      } else {
        base64.push('=');
      }

      if let Some(idx) = fourth_idx {
        base64.push(TABLE[idx as usize]);
      } else {
        base64.push('=');
      }

      base64
    })
}


pub fn decode_bytes(base64: &[u8]) -> Result<Vec<u8>, &str> {
  let map = reverse_table();

  if base64.iter().any(|byte| !map.contains_key(&(*byte as char)) && *byte != b'=' && *byte != b'\n') {
    return Err("detected character that is not allowed in base64");
  }

  let clean_base64 = base64
    .iter()
    .filter(|&c| *c != b'\n')
    .collect::<Vec<_>>();

  if clean_base64.len() % 4 != 0 {
    return Err("invalid length for base64");
  }

  Ok(clean_base64
     .iter()
     .take_while(|&c| **c != b'=')
     .map(|&b| map.get(&(*b as char)).unwrap())
     .collect::<Vec<_>>()
     .as_slice()
     .chunks(4)
     .fold(Vec::with_capacity(base64.len() * 3 / 4), |mut bytes, chunk| {
       bytes.push(((chunk[0] << 2) + (chunk[1] >> 4)));

       if chunk.len() > 2 {
         bytes.push(((chunk[1] << 4) + (chunk[2] >> 2)));
       }

       if chunk.len() > 3 {
         bytes.push(((chunk[2] << 6) + chunk[3]));
       }

       bytes
     }))
}

pub fn decode(base64: &str) -> Result<Vec<u8>, &str> {
  decode_bytes(base64.as_bytes())
}

#[cfg(test)]
mod tests {
  mod encode {
    use super::super::encode;

    #[test]
    fn handles_empty_byte_sequence() {
      assert_eq!(encode(&[]), "");
    }

    #[test]
    fn handles_arbitrary_byte_sequences() {
      assert_eq!(encode(&[77, 97, 110]), "TWFu");
    }

    #[test]
    fn handles_padding() {
      assert_eq!(encode(&[77]), "TQ==");
      assert_eq!(encode(&[77, 97]), "TWE=");
      assert_eq!(encode(&[77, 97, 110, 77, 97]), "TWFuTWE=");
    }
  }

  mod decode {
    use super::super::decode;

    #[test]
    fn handles_empty_base64_string() {
      assert_eq!(decode(""), Ok(vec![]));
    }

    #[test]
    fn rejects_improper_base64() {
      assert!(decode("©").is_err());
      assert!(decode("abc").is_err());
    }

    #[test]
    fn handles_non_padded_base64() {
      assert_eq!(decode("TWFu"), Ok(vec![77, 97, 110]));
    }

    #[test]
    fn handles_padded_base64() {
      assert_eq!(decode("TQ=="), Ok(vec![77]));
      assert_eq!(decode("TWE="), Ok(vec![77, 97]));
      assert_eq!(decode("TWFuTWE="), Ok(vec![77, 97, 110, 77, 97]));
    }

    #[test]
    fn handles_line_breaks() {
      let base64 = "
TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlz
IHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2Yg
dGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGlu
dWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRo
ZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=";

      let data = b"Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.";

      assert_eq!(decode(base64), Ok(data.to_vec()));
    }
  }
}
