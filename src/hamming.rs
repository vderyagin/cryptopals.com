pub fn distance(a: &[u8], b: &[u8]) -> usize {
  if a.len() != b.len() {
    panic!("Provided inputs are of different sizes")
  }

  a
    .iter()
    .zip(b)
    .map(|(ab, bb)| (ab ^ bb).count_ones() as usize)
    .sum()
}

#[cfg(test)]
mod tests {
  use super::distance;

  #[test]
  #[should_panic]
  fn panics_with_sequences_of_different_size() {
    distance(b"foo", b"x");
  }

  #[test]
  fn is_zero_for_two_empty_byte_sequences() {
    assert_eq!(distance(&[], &[]), 0);
  }

  #[test]
  fn is_zero_for_two_equal_non_empty_sequences() {
    assert_eq!(distance(b"garply", b"garply"), 0);
  }

  #[test]
  fn is_not_zero_for_two_different_non_empty_sequences() {
    assert!(distance(b"yoyoyo", b"garply") != 0);
  }

  #[test]
  fn works_for_provided_example() {
    assert_eq!(distance(b"this is a test", b"wokka wokka!!!"), 37);
  }
}
