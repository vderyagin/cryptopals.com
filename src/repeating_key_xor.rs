pub fn encrypt(plaintext: &[u8], key: &[u8]) -> Vec<u8> {
  plaintext
    .iter()
    .zip(key.iter().cycle())
    .map(|(p, k)| p ^ k)
    .collect()
}
