use byteorder::{LittleEndian, WriteBytesExt};
use aes::ecb;
use utils;

const BLOCK_SIZE: usize = 16;

pub fn encrypt(data: &[u8], key: &[u8], nonce: u64) -> Vec<u8> {
  let mut keystream_block_template = Vec::with_capacity(BLOCK_SIZE);
  keystream_block_template.write_u64::<LittleEndian>(nonce).unwrap();

  data
    .chunks(BLOCK_SIZE)
    .zip((0..).map(|counter| {
      let mut block = keystream_block_template.to_vec();
      block.write_u64::<LittleEndian>(counter).unwrap();
      ecb::encrypt(&block, key)
    }))
    .map(|(input_block, keystream_block)| utils::xor(&keystream_block, input_block))
    .collect::<Vec<_>>()
    .concat()
}

#[cfg(test)]
mod tests {
  use super::encrypt;

  #[test]
  fn encrypts_and_decrypts_stuff_sized_in_multiples_of_block_size() {
    let data = b"YELLOW SUBMARINEYELLOW SUBMARINEYELLOW SUBMARINE";
    let key = b"YELLOW SUBMARINE";
    let nonce = 77;
    assert_eq!(encrypt(&encrypt(data, key, nonce), key, nonce),
               data.to_vec());
  }

  #[test]
  fn deals_with_stuff_that_is_not_aligned_with_block_size() {
    let data = b"YELLOW SUBMARINE YELLOW SUBMARINE YELLOW";
    let key = b"YELLOW SUBMARINE";
    let nonce = 2382377;
    assert_eq!(encrypt(&encrypt(data, key, nonce), key, nonce),
               data.to_vec());
  }

  #[test]
  fn does_not_require_any_padding() {
    let data = b"foo";
    let key = b"YELLOW SUBMARINE";
    let nonce = 12345;
    assert_eq!(encrypt(data, key, nonce).len(),
               data.len());
  }
}
