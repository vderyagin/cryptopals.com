use openssl::crypto::symm::{Crypter, Type, Mode};
use pkcs7;
use utils;

const BLOCK_SIZE: usize = 16;

fn transform(input: &[u8], key: &[u8], mode: Mode) -> Vec<u8> {
  let size = input.len() + BLOCK_SIZE;
  let mut cipher = Crypter::new(Type::AES_128_ECB, mode, key, None).unwrap();
  cipher.pad(false);
  let mut output = Vec::with_capacity(size);
  let mut bytes_written = 0;
  output.resize(size, 0);
  bytes_written += cipher.update(input, &mut output).unwrap();
  bytes_written += cipher.finalize(&mut output[bytes_written..]).unwrap();
  output.truncate(bytes_written);
  output
}

pub fn encrypt(ciphertext: &[u8], key: &[u8]) -> Vec<u8> {
  transform(ciphertext, key, Mode::Encrypt)
}

pub fn decrypt(ciphertext: &[u8], key: &[u8]) -> Vec<u8> {
  transform(ciphertext, key, Mode::Decrypt)
}

/// Recover a suffix `encryptor` appends to data that is passed to it
pub fn recover_suffix(encryptor: &Box<Fn(&[u8]) -> Vec<u8>>) -> Vec<u8> {
  let unknown_data_len = encryptor(&[]).len();

  let mut initial_padding = Vec::with_capacity(BLOCK_SIZE);
  let mut recovered = Vec::with_capacity(unknown_data_len);
  let mut plaintext = Vec::with_capacity(unknown_data_len + BLOCK_SIZE);

  while recovered.len() < unknown_data_len {
    let current_block = recovered.len() / BLOCK_SIZE;
    let initial_padding_size = BLOCK_SIZE - recovered.len() % BLOCK_SIZE - 1;
    initial_padding.resize(initial_padding_size, 0);
    let reference_ciphertext = encryptor(&initial_padding);
    let reference_block =
      utils::extract_block(&reference_ciphertext, current_block, BLOCK_SIZE);

    plaintext.clear();
    plaintext.extend_from_slice(&initial_padding);
    plaintext.extend_from_slice(&recovered);
    plaintext.push(0);
    let unknown_byte_idx = plaintext.len() - 1;

    let next_byte = (0..256)
      .map(|number| number as u8)
      .find(|byte| {
        plaintext[unknown_byte_idx] = *byte;
        let test_ciphertext = encryptor(&plaintext);
        let test_block =
          utils::extract_block(&test_ciphertext, current_block, BLOCK_SIZE);
        test_block == reference_block
      });

    if let Some(byte) = next_byte {
      recovered.push(byte);
    } else {
      break;
    }
  }

  pkcs7::unpad(&recovered).to_vec()
}

/// determines if `func` encrypts data using ECB mode
pub fn oracle(func: Box<Fn(&[u8]) -> Vec<u8>>) -> bool {
  let size = BLOCK_SIZE * 100;
  let mut plaintext = Vec::with_capacity(size);
  plaintext.resize(size, 0);
  let ciphertext = func(plaintext.as_slice());
  utils::count_duplicate_blocks(&ciphertext, BLOCK_SIZE) >= 50
}


#[cfg(test)]
mod tests {
  use super::{encrypt, decrypt};

  #[test]
  fn encrypts_and_decrypts_stuff() {
    let data = b"YELLOW SUBMARINEYELLOW SUBMARINEYELLOW SUBMARINE";
    let key = b"YELLOW SUBMARINE";
    assert_eq!(decrypt(&encrypt(data, key), key),
               data.to_vec());
  }
}
