use aes::ecb;
use utils;
use pkcs7;

const BLOCK_SIZE: usize = 16;

pub fn encrypt(plaintext: &[u8], key: &[u8], iv: &[u8]) -> Vec<u8> {
  let mut ciphertext = Vec::with_capacity(plaintext.len());

  plaintext
    .chunks(BLOCK_SIZE)
    .fold(iv.to_vec(), |iv, chunk| {
      let ciphertext_block = ecb::encrypt(&utils::xor(chunk, &iv), key);
      ciphertext.extend_from_slice(&ciphertext_block);
      ciphertext_block
    });

  ciphertext
}

pub fn decrypt(ciphertext: &[u8], key: &[u8], iv: &[u8]) -> Vec<u8> {
  let mut plaintext = Vec::with_capacity(ciphertext.len());

  ciphertext
    .chunks(BLOCK_SIZE)
    .fold(iv.to_vec(), |iv, chunk| {
      plaintext.extend(utils::xor(&ecb::decrypt(chunk, key), &iv));
      chunk.to_vec()
    });

  plaintext
}

pub fn decrypt_block_with_padding_oracle(cipherblock: &[u8], iv: &[u8], padding_oracle: &Box<Fn(&[u8], &[u8]) -> bool>) -> Vec<u8> {
  if cipherblock.len() != BLOCK_SIZE {
    panic!("This function works on single blocks only")
  }

  let mut guessed_bytes = Vec::with_capacity(BLOCK_SIZE);
  guessed_bytes.resize(BLOCK_SIZE, 0);

  for position in (0..BLOCK_SIZE).rev() {
    let padding_byte = (BLOCK_SIZE - position) as u8;

    for byte in (0..256).map(|n| n as u8).rev() {
      let mut scratch = iv.to_vec();
      guessed_bytes[position] = byte;

      for idx in position..BLOCK_SIZE {
        scratch[idx] ^= guessed_bytes[idx] ^ padding_byte;
      }

      if padding_oracle(cipherblock, &scratch) {
        break;
      }
    }
  }

  guessed_bytes
}

pub fn make_padding_oracle(key: &[u8]) -> Box<Fn(&[u8], &[u8]) -> bool> {
  let key = key.to_vec();

  Box::new(move |ciphertext, iv| {
    let padded_plaintext = decrypt(ciphertext, &key, iv);
    pkcs7::padding_is_valid(&padded_plaintext)
  })
}

#[cfg(test)]
mod tests {
  mod decrypt_blocks_with_padding_oracle {
    use utils;
    use super::super::{
      encrypt,
      decrypt_block_with_padding_oracle as decrypt,
      BLOCK_SIZE,
      make_padding_oracle
    };

    #[test]
    fn recovers_plaintext_using_padding_oracle() {
      let key = utils::random_bytes(BLOCK_SIZE);
      let iv = utils::random_bytes(BLOCK_SIZE);
      let oracle = make_padding_oracle(&key);

      for plaintext_block in (0..10).map(|_| utils::random_bytes(BLOCK_SIZE)) {
        let cipherblock = encrypt(&plaintext_block, &key, &iv);
        assert_eq!(decrypt(&cipherblock, &iv, &oracle), plaintext_block);
      }
    }
  }
}
