pub fn pad(data: &[u8], block_size: usize) -> Vec<u8> {
  let padding_byte = (block_size - data.len() % block_size) as u8;
  let mut padded = Vec::with_capacity(data.len() + padding_byte as usize);
  padded.extend_from_slice(data);
  for _ in 0..padding_byte {
    padded.push(padding_byte)
  }
  padded
}

pub fn unpad(data: &[u8]) -> &[u8] {
  if data.is_empty() {
    panic!("can't unpad empty data");
  }

  let padding_length = *data.last().unwrap() as usize;
  let data_size = data.len() - padding_length;

  if data[data_size..].iter().any(|&byte| byte != padding_length as u8) {
    panic!("data is not properly padded");
  }

  &data[..data_size]
}

pub fn padding_is_valid(data: &[u8]) -> bool {
  if data.is_empty() {
    panic!("input is empty")
  }

  let &last_byte = data.last().unwrap();

  if last_byte == 0 {
    return false;
  }

  data.iter().rev().take(last_byte as usize).all(|&byte| byte == last_byte)
}

#[cfg(test)]
mod tests {
  mod pad {
    use super::super::pad;

    #[test]
    fn pads_things() {
      assert_eq!(&pad(b"FOOBARBAZ", 16), b"FOOBARBAZ\x07\x07\x07\x07\x07\x07\x07");
    }

    #[test]
    fn pads_data_whose_size_is_multiple_of_block_size() {
      assert_eq!(&pad(b"YELLOW SUBMARINE", 16),
                 b"YELLOW SUBMARINE\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10");
    }
  }

  mod unpad {
    use super::super::unpad;

    #[test]
    fn removes_padding() {
      assert_eq!(unpad(b"YELLOW SUBMARINE\x04\x04\x04\x04"),
                 b"YELLOW SUBMARINE");
    }

    #[test]
    #[should_panic]
    fn panics_if_data_is_not_properly_padded() {
      unpad(b"YELLOW SUBMARINE\x03\x03");
      unpad(b"YELLOW SUBMARINE");
    }

    #[test]
    fn removes_padding_from_data_whose_size_is_multiple_of_block_size() {
      assert_eq!(unpad(b"YELLOW SUBMARINE\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10"),
                 b"YELLOW SUBMARINE");
    }
  }

  mod padding_is_valid {
    use super::super::padding_is_valid;

    #[test]
    fn accepts_valid_paddings() {
      assert!(padding_is_valid(b"bla-bla-bla-bla\x01"));
      assert!(padding_is_valid(b"bla-bla-bla-bl\x02\x02"));
      assert!(padding_is_valid(b"bla-bla-bla-b\x03\x03\x03"));
      assert!(padding_is_valid(b"bla-bla-bla-\x04\x04\x04\x04"));
    }

    #[test]
    fn rejects_invalid_paddings() {
      assert!(!padding_is_valid(b"bla-bla-bla-bla\x00"));
      assert!(!padding_is_valid(b"bla-bla-bla-bla-"));
      assert!(!padding_is_valid(b"bla-bla-bla-b\x01\x02\x03"));
    }
  }
}
